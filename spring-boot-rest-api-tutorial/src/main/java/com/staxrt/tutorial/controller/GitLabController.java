package com.staxrt.tutorial.controller;

import com.staxrt.tutorial.model.CommitRequest;
import com.staxrt.tutorial.model.CreateBranchRequest;
import com.staxrt.tutorial.model.CreateMrRequest;
import com.staxrt.tutorial.service.GitLabService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/git/test/v1")
public class GitLabController {
    @Autowired
    private GitLabService gitlabservice;

    @GetMapping("/groups")
    public String getAllGroup() {

        return gitlabservice.getAllGroup();
    }

    @GetMapping("/projects")
    public String getAllProjects() {
        return gitlabservice.getProjectById();
    }


    @PostMapping("/merge_requests")
    public String postMergeRequests(@RequestBody CreateMrRequest request) {
        return gitlabservice.postMergeRequest(request);
    }

    @GetMapping("/branches")
    public String getBranchesWithProjectId() {
        return gitlabservice.getAllBranchesWithProjectId();
    }

    @PostMapping("/branches")
    public String postBranch(@RequestBody CreateBranchRequest request) {
        return gitlabservice.createBranch(request);
    }

    @PostMapping("/commits")
    public String postCommits(@RequestBody CommitRequest request) {
        return gitlabservice.postCommits(request);
    }





}
