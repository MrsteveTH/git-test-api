package com.staxrt.tutorial.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Action implements Serializable {
    private String action;
    private String file_path;
    private String content;
    private String previous_path;
    private String execute_filemode;
}
