package com.staxrt.tutorial.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class CreateMrRequest implements Serializable {
    private String id;
    private String source_branch;
    private String target_branch;
    private String title;
    private String description;
    private Integer assignee_id;
    private Integer project_id;
    private Boolean remove_source_branch;
}
