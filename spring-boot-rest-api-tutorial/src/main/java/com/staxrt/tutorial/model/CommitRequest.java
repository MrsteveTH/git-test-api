package com.staxrt.tutorial.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CommitRequest implements Serializable {
    private String branch;
    private String commit_message;
    private List<Action> actions;
}
