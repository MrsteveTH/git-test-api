package com.staxrt.tutorial.model;

import lombok.Data;
import lombok.Getter;

import java.io.Serializable;

@Data
@Getter
public class CreateBranchRequest implements Serializable {
    private Integer id;
    private String branch;
    private String ref;

    public String getBranch() {
        return branch;
    }

    public String getRef() {
        return ref;
    }
}
