package com.staxrt.tutorial.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Assignee implements Serializable {
    private int id;
    private String name;
    private String username;
}
