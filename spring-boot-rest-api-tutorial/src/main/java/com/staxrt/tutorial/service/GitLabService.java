package com.staxrt.tutorial.service;

import com.staxrt.tutorial.model.CommitRequest;
import com.staxrt.tutorial.model.CreateBranchRequest;
import com.staxrt.tutorial.model.CreateMrRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class GitLabService {

    @Value("${gitlab.inquiry.group.url}")
    private String gitLabInquiryGroupUrl;

    @Value("${gitlab.inquiry.projects.url}")
    private String gitLabInquiryProjectUrl;

    @Value("${gitlab.inquiry.branch.url}")
    private String gitLabInquiryBranchUrl;

    @Value("${gitlab.inquiry.merge-request.url}")
    private String gitLabInquiryMergeRequestsUrl;

    @Value("${gitlab.create.merge-request.url}")
    private String gitLabCreateMergeRequestsUrl;

    @Value("${gitlab.commit.url}")
    private String gitLabCommitsUrl;

    @Value("${gitlap.auth.user.token}")
    private String AUTH_USER_TOKEN;

    @Value("${gitlap.projectId}")
    private String PROJECT_ID;

    public String getAllGroup() {
        RestTemplate rest = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> response = rest.exchange(
                gitLabInquiryGroupUrl,
                HttpMethod.GET,
                httpEntity,
                String.class);
        return response.getBody();
    }

    public String getProjectById() {
        RestTemplate rest = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        Map<String, String> pathVariable = new HashMap<>();
        pathVariable.put("projectId", PROJECT_ID);
        httpHeaders.set("PRIVATE-TOKEN", AUTH_USER_TOKEN);
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> response = rest.exchange(
                gitLabInquiryProjectUrl,
                HttpMethod.GET,
                httpEntity,
                String.class,
                pathVariable);
        return response.getBody();
    }

    public String getAllBranchesWithProjectId() {
        RestTemplate rest = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        Map<String, String> pathVariable = new HashMap<>();
        pathVariable.put("projectId", PROJECT_ID);
        httpHeaders.set("PRIVATE-TOKEN", AUTH_USER_TOKEN);
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);
        System.out.println("Start calling get Branch with Project ID");
        ResponseEntity<String> response = rest.exchange(
                gitLabInquiryBranchUrl,
                HttpMethod.GET,
                httpEntity,
                String.class,
                pathVariable);
        return response.getBody();
    }

    public String createBranch(CreateBranchRequest request) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(gitLabInquiryBranchUrl)
                // Add query parameter
                .queryParam("branch", request.getBranch())
                .queryParam("ref", request.getRef());
        RestTemplate rest = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        Map<String, String> pathVariable = new HashMap<>();
        pathVariable.put("projectId", PROJECT_ID);
        httpHeaders.set("PRIVATE-TOKEN", AUTH_USER_TOKEN);
        HttpEntity httpEntity = new HttpEntity<>(request,httpHeaders);
        System.out.println("Start calling get Branch with Project ID");
        ResponseEntity<String> response = rest.exchange(
                builder.build().toUriString(),
                HttpMethod.POST,
                httpEntity,
                String.class,
                pathVariable);
        return response.getBody();
    }

    public String getMergeRequestAll() {
        RestTemplate rest = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        Map<String, String> pathVariable = new HashMap<>();
        pathVariable.put("projectId", PROJECT_ID);
        httpHeaders.set("PRIVATE-TOKEN", AUTH_USER_TOKEN);
        HttpEntity httpEntity = new HttpEntity<>(httpHeaders);
        System.out.println("Start calling get Merge Request");
        ResponseEntity<String> response = rest.exchange(
                gitLabInquiryMergeRequestsUrl,
                HttpMethod.GET,
                httpEntity,
                String.class,
                pathVariable);
        return response.getBody();
    }

    public String postMergeRequest(CreateMrRequest request) {
        RestTemplate rest = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        Map<String, String> pathVariable = new HashMap<>();
        pathVariable.put("projectId", PROJECT_ID);
        httpHeaders.set("PRIVATE-TOKEN", AUTH_USER_TOKEN);
        HttpEntity httpEntity = new HttpEntity<>(request,httpHeaders);
        System.out.println("Start calling Create Merge Request");
        ResponseEntity<String> response = rest.exchange(
                gitLabCreateMergeRequestsUrl,
                HttpMethod.POST,
                httpEntity,
                String.class,
                pathVariable);
        System.out.println("Created Merge Request");
        return response.getBody();
    }

    public String postCommits(CommitRequest request) {
        RestTemplate rest = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        Map<String, String> pathVariable = new HashMap<>();
        pathVariable.put("projectId", PROJECT_ID);
        httpHeaders.set("PRIVATE-TOKEN", AUTH_USER_TOKEN);
        HttpEntity httpEntity = new HttpEntity<>(request,httpHeaders);
        System.out.println("Start calling Commits Request");
        ResponseEntity<String> response = rest.exchange(
                gitLabCommitsUrl,
                HttpMethod.POST,
                httpEntity,
                String.class,
                pathVariable);
        System.out.println("Done Commit file.");
        return response.getBody();
    }

}
